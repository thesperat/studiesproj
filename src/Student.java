public class Student extends Osoba{
    private String numerIndeksu;

    public Student(String numerIndeksu, String imie, String nazwisko, int rokUrodzenia){
        super(imie, nazwisko, rokUrodzenia);
        this.numerIndeksu = numerIndeksu;
    }

    public String getNumerIndeksu(){
        return this.numerIndeksu;
    }

}
