public class SortingOperations {
    public void sortBySka(Student[] studentsArray){
        Student tempValue;
        String ska1, ska2;
        int intSka1, intSka2;
        for(int i = 0; i < studentsArray.length; i++){
            for(int j = i+1; j < studentsArray.length; j++){
                tempValue=null;
                ska1 = studentsArray[i].getNumerIndeksu();
                ska2 = studentsArray[j].getNumerIndeksu();
                intSka1 = Integer.parseInt(ska1.substring(1));
                intSka2 = Integer.parseInt(ska2.substring(1));
                if(intSka1 > intSka2){
                    tempValue = studentsArray[i];
                    studentsArray[i] = studentsArray[j];
                    studentsArray[j] = tempValue;
                }
            }
        }
    }
}
