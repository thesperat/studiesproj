import java.io.*;

public class DataObtainer {
   private String path = "C:\\Users\\mmagda\\Desktop\\";
   private String file1 = "dane01.txt";
   private String file2 = "dane02.txt";

    public Student[] loadItems() throws IOException {
        int file1Lines = 0;
        int file2Lines = 0;
        int arrayStp = 0;
        String splitedInfo[];

        BufferedReader reader1 = new BufferedReader(new FileReader(new File(this.path + this.file1)));
        BufferedReader reader2 = new BufferedReader(new FileReader(new File(this.path + this.file2)));
        BufferedReader reader3 = new BufferedReader(new FileReader(new File(this.path + this.file1)));
        BufferedReader reader4 = new BufferedReader(new FileReader(new File(this.path + this.file2)));

        while(reader1.readLine() != null) file1Lines++;
        while(reader2.readLine() != null) file2Lines++;
        reader1.close();
        reader2.close();


        Student[] studentsArray = new Student[file1Lines + file2Lines];

        String line;
        while((line = reader3.readLine()) != null){
            splitedInfo = line.split(" ");
            Student std = new Student(splitedInfo[0], splitedInfo[1], splitedInfo[2], Integer.parseInt(splitedInfo[3]));
            studentsArray[arrayStp++] = std;
            System.out.println("Append");

        }
        while((line=reader4.readLine()) != null){
            splitedInfo = line.split(" ");
            Student std = new Student(splitedInfo[0], splitedInfo[1],  splitedInfo[2], Integer.parseInt(splitedInfo[3]));
            studentsArray[arrayStp++] = std;
            System.out.println("Append2");
        }

        reader3.close();
        reader4.close();
        return studentsArray;
    }

    public void writeToFile(Student[] studentsArray, String file) throws IOException{
        StringBuilder data = new StringBuilder();
        PrintWriter out = new PrintWriter(this.path + file);
        for(Student std: studentsArray){
            data.append(std.getNumerIndeksu() + " " +
                    std.getImie() + " " +
                    std.getNazwisko() + " " +
                    std.getRokUrodzenia() + "\n");

        }
        out.print(data.toString());
        out.close();
    }
}


