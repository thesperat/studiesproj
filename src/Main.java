import java.io.IOException;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        DataObtainer dataObtainer = new DataObtainer();
        SortingOperations sortingOperations = new SortingOperations();
        Student[] studentArray = null;
        try {
            studentArray = dataObtainer.loadItems();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(Arrays.toString(studentArray));
        sortingOperations.sortBySka(studentArray);
        System.out.println(Arrays.toString(studentArray));
        try {
            dataObtainer.writeToFile(studentArray, "sortSka.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
